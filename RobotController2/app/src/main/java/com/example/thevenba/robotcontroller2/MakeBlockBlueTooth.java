package com.example.thevenba.robotcontroller2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import fr.iutvalence.android.BTConnectionHandlerLib.BTConnectionHandler;
import fr.iutvalence.android.BTConnectionHandlerLib.exceptions.BTHandlingException;


public class MakeBlockBlueTooth extends ActionBarActivity {

    private BTConnectionHandler connectionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_block_blue_tooth);
        Button button = (Button) findViewById(R.id.buttonJoystick);
        button.setText("Joystick");
        connectionHandler = new BTConnectionHandler(getBaseContext());
    }

    public void buttonOnclickListener(View view){
        Intent intent = new Intent(this, Joystick.class);
        startActivity(intent);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_make_block_blue_tooth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickConnect(View v){
        EditText editText = (EditText) findViewById(R.id.editTextDeviceName);
        try {
            connectionHandler.connectToBTDevice(editText.getText().toString());
        } catch (BTHandlingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickDisconnect(View v){
        connectionHandler.closeConnection();
    }

    public void onClickSend(View v){
        EditText editText = (EditText) findViewById(R.id.editTextCommand);
        try {
            connectionHandler.sendData(editText.getText().toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }

    public void onClickFront(View v){
        try {
            connectionHandler.sendData("A");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }

    public void onClickLeft(View v){
        try {
            connectionHandler.sendData("G");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }

    public void onClickRight(View v){
        try {
            connectionHandler.sendData("D");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }

    public void onClickBack(View v){
        try {
            connectionHandler.sendData("R");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }

    public void onClickStop(View v){
        try {
            connectionHandler.sendData("S");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }
    }
}
